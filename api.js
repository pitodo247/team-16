const { Router } = require("express");
//const { MongoClient, ObjectId } = require("mongod");
const passport = require("passport");
const LocalStrategy = require("passport-local");

//const Gambler = require("./models/Gambler");
const apiRouter = Router();

apiRouter.get("/user/:id", async (req, res) => {
  try {
    const searchedGambler = await Gambler.findById(req.params.id);
    const pojoGambler = searchedGambler.toObject();
    delete pojoGambler.password;
    console.log("2", pojoGambler);
    res.json(pojoGambler);
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});

apiRouter.get("/user/:id/hash", async (req, res) => {
  try {
    const searchedGambler = await Gambler.findById(req.params.id);
    const pojoGambler = searchedGambler.toObject();
    res.json({ hash: pojoGambler.password });
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.post("/user/", async (req, res) => {
  try {
    const newGambler = await Gambler({ ...req.body });
    await newGambler.save();
    res.json({ id: newGambler._id });
  } catch (err) {
    console.error(err);
    res.sendStatus(400);
  }
});

passport.use(
  new LocalStrategy(async function verify(username, password, cb) {
    try {
      const foundUser = await Gambler.findOne({ username: username });
      if (!foundUser) {
        return cb(null, false, { message: "Incorrect username or password." });
      }
      foundUser.validatePassword(password, cb);
    } catch (err) {
      return cb(err);
    }
  })
);

passport.serializeUser(function (user, cb) {
  console.log(user);
  process.nextTick(function () {
    cb(null, { id: user._id, username: user.username });
  });
});

passport.deserializeUser(function (user, cb) {
  process.nextTick(function () {
    return cb(null, user);
  });
});

apiRouter.post(
  "/login/password",
  passport.authenticate("local", {
    successRedirect: "/homePage",
    failureRedirect: "/",
  })
);

module.exports = apiRouter;
