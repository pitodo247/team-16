const { MongoClient } = require("mongodb");
console.log(process.env.MODE);
//let url = "mongodb://192.168.171.67:27017";
let url = "mongodb://127.0.0.1:27017";
if (process.env.MODE == "production") {
  url = `mongodb://team16:${process.env.MONGO_PASSWORD}@192.168.171.67`;
} else {
  //url = "mongodb://192.168.171.67:27017";
  url = "mongodb://127.0.0.1:27017";
}
const mongoClient = new MongoClient(url);
module.exports = mongoClient;
