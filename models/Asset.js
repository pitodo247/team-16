const mongoose = require("mongoose");
const { RandomThoughts } = require(".");

const assetSchema = new mongoose.Schema({
  symbol: { type: String, required: true },
  shares: { type: Number, required: true },
  type: { type: String, required: true },
});

const Asset = mongoose.model("Asset", assetSchema);
module.exports = RandomThoughts;
