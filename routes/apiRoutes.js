const { Router } = require("express");
const userRouter = Router();
const apiRouter = Router();
const { User, Asset } = require("../models");
//const fetch = require("node-fetch");
const finnhubToken = process.env.FINNHUB_TOKEN;

userRouter.get("/:id", async (req, res) => {
  try {
    const searchedUser = await User.findById(req.params.id).populate("assets");
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.post("/", async (req, res) => {
  try {
    const newUser = await User({
      ...req.body,
      balance: 6789,
      assets: [],
    });
    const savedUser = await newUser.save();
    res.json({ id: savedUser._id });
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.use("/user", userRouter);

module.exports = apiRouter;
