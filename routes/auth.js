const { Router } = require("express");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { User } = require("../models/");
const router = new Router();




// passport.use(new LocalStrategy(
//   function(username, password, done) {
//     User.findOne({ username: username }, function(err, user) {
//       if (err) { return done(err); }
//       if (!user) {
//         return done(null, false, { message: 'Incorrect username.' });
//       }
//       if (!user.validPassword(password)) {
//         return done(null, false, { message: 'Incorrect password.' });
//       }
//       return done(null, user);
//     });
//   }
// ));
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      if (!user.validPassword(password)) { return done(null, false); }
      return done(null, user);
    });
  }
));

passport.serializeUser(function (user, cb) {
  console.log(user);
  process.nextTick(function () {
    cb(null, { id: user._id, username: user.username });
  });
});

passport.deserializeUser(function (user, cb) {
  process.nextTick(function () {
    return cb(null, user);
  });
});

router.post("/register",async (req,res)=>{
  const {username,password, securityQuestion,
    securityAnswer,
    RandomThoughts} = req.body;
 
    try {
      const user =await User.findOne({username});
      console.log(user)
        if (!user) {
          // Create new user
          const newUser = new User({
      username,
      password,
      securityQuestion,
      securityAnswer,
      RandomThoughts
          })
          const data = await newUser.save();
          // res.redirect("/logIN");
       
          res.status(200).json({
            status:"success",
            message:"User Created Succesfully."
          })
        }else{
          res.status(422).json({
            status:"failed",
            message:"User Already Exist."
          })
        }
      
    } catch (error) {
      console.log(error)
    }

  // passport.authenticate('local', { successRedirect: '/',
  // failureRedirect: '/login',
  // failureFlash: true })
})

router.post("/login",async (req,res)=>{
  const {username,password} = req.body;
  console.log(username,password)
const user = await User.findOne({username})

if(user){
  res.status(200).json({
    status:"success",
    message:"User Login Succesfully."
  })
}else{
  res.status(422).json({
    status:"failed",
    message:"Invalid Input."
  })
}
  // passport.authenticate('local', { successRedirect: '/',
  // failureRedirect: '/login',
  // failureFlash: true })
})

module.exports = router;
