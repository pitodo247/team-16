const router = require("express").Router();
const path = require("path");

const serveLandingPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
};

const servePortfolioPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
};

router.get("/contact", serveLandingPage);

router.get("/about", serveLandingPage);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect("/logIN");
}
router.get("/about", ensureAuthenticated, servePortfolioPage);

router.get("/login", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/logIN.html"));
});

module.exports = router;
