const express = require("express");
const bodyParser = require("body-parser");
require("dotenv").config();
const app = express();
const port = 8115;
const morgan = require("morgan");
const viewRoutes = require("./routes/viewRoutes");
const apiRoutes = require("./routes/apiRoutes");
const authRoutes = require("./routes/auth");
const mongoose = require("mongoose");
const path = require('path');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
//const api = require("./api");

var session = require("express-session");
const MongoStore = require("connect-mongo");

if (process.env.MODE == "production") {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({
        mongoUrl: `mongodb://team16:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/team16?authSource=admin`,
      }),
    })
  );
} else {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({ mongoUrl: "mongodb://127.0.0.1:27017/goose" }),
      //store: `mongodb://team16:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/team16?authSource=admin`
    })
  );
}
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// function ensureAuthenticated(req, res, next) {
//   if (req.isAuthenticated()) {
//     return next();
//   }
//   return res.redirect("/");
// }

const servehomePage = (req, res) => {
  res.sendFile(path.join(__dirname, "./public/homePage.html"));
};
app.get("/homePage", servehomePage);
app.get("/homePage.html", servehomePage);

app.get("/index", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/homePage.html"));
});
app.get("/extra", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/extraPage.html"));
});
app.get("/register", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/register.html"));
});

const aLoggerMiddleware = (req, res, next) => {
  console.log(req.method, req.url);
  next();
};
app.use(aLoggerMiddleware);


passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      if (!user.validPassword(password)) { return done(null, false); }
      return done(null, user);
    });
  }
));

passport.serializeUser(function (user, cb) {
  console.log(user);
  process.nextTick(function () {
    cb(null, { id: user._id, username: user.username });
  });
});

passport.deserializeUser(function (user, cb) {
  process.nextTick(function () {
    return cb(null, user);
  });
});



app.use(viewRoutes);
app.use("/api", apiRoutes);
app.use("/api/auth",authRoutes);
app.use(express.static("public"), express.static("dist"));

//app.use("/api", api);
async function main() {
  if (process.env.MODE == "production") {
    await mongoose.connect(`mongodb://192.168.171.67:27017/team16`, {
      useNewUrlParser: true,
      authSource: "admin",
      user: "team16",
      pass: process.env.MONGO_PASSWORD,
    });
  } else {
    //await mongoose.connect("mongodb://192.168.171.67:27017/goose");
    await mongoose.connect("mongodb://127.0.0.1:27017/goose");
  }
  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
}

main().catch((err) => console.error(err));
